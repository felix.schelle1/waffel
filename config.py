#!/usr/bin/env python3

RUNDIR = "/home/asterix/projects/waffel/"
DATADIR = "/home/asterix/projects/waffel/data/"
FILES = {
        "thanks": "thanks.html",
        "index": "index.html"
        }

if __name__ == "__main__":
    print(f"RUNDIR = {RUNDIR}\nDATADIR = {DATADIR}")
