#!/usr/bin/env python3

import bottle
import os

from config import RUNDIR, DATADIR, FILES

DEBUG_LEVEL = 2

if DEBUG_LEVEL >= 2:
    print(f"loaded config: RUNDIR = {RUNDIR}, DATADIR = {DATADIR}")

app = application = bottle.Bottle()


@app.route('/')
def index():
    """
    serve index.html
    :return: index.html
    """
    return bottle.static_file(FILES["index"], root=RUNDIR)


@app.route('/<filename:path>')
def send_static(filename):
    """
    serve static files
    :param filename: path to file
    :return: static file
    """
    return bottle.static_file(filename, root=RUNDIR)


@app.post('/kandidatur.html')
def store_kandidatur():
    """
    stores kandidatur of a studi
    :return: thanks page
    """
    if not os.path.exists(DATADIR):
        try:
            os.mkdir(DATADIR)
        except OSError:
            return f"Server Error: could not create DATADIR {DATADIR}. Answers can not be saved."
    values = []
    # split data into gremien
    for value in bottle.request.forms.decode().values():
        values.append(value.replace(';', ''))
    # each gremium one file? each list in each gremium one file?
    with open(DATADIR+"kandidatur.csv", 'a+') as f:
        f.write(";".join(values)+"\n")
    return bottle.static_file(FILES["thanks"], root=RUNDIR)


@app.post('/listenvorschlag.html')
def store_listenvorschlag():
    """
    stores listenvorschlag of listenverantwortliche
    :return: thanks page
    """
    if not os.path.exists(DATADIR):
        try:
            os.mkdir(DATADIR)
        except OSError:
            return f"Server Error: could not create DATADIR {DATADIR}. Answers can not be saved."
    values = []
    for value in bottle.request.forms.decode().values():
        values.append(value.replace(';', ''))
    # one file for each gremium
    with open(DATADIR+"listenvorschlag.csv", 'a+') as f:
        f.write(";".join(values)+"\n")
    return bottle.static_file(FILES["thanks"], root=RUNDIR)


@app.post('/fg.html')
def store_fg():
    """
    stores listenvorschlag of listenverantwortliche
    :return: thanks page
    """
    if not os.path.exists(DATADIR):
        try:
            os.mkdir(DATADIR)
        except OSError:
            return f"Server Error: could not create DATADIR {DATADIR}. Answers can not be saved."
    values = []
    for value in bottle.request.forms.decode().values():
        values.append(value.replace(';', ''))
    # one file for each fg?
    with open(DATADIR+"fg.csv", 'a+') as f:
        f.write(";".join(values)+"\n")
    return bottle.static_file(FILES["thanks"], root=RUNDIR)


if __name__ == "__main__":
    app.run(host='localhost', port=8080, debug=True)

