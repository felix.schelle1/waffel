# WAFFEL

**W**ahl**a**ngaben**f**ormular **f**ür **e**ingereichte **L**isten

Automatisierung, Digitlisierung und Vereinfachung der bürokratischen Aufwände bei der Wahlvorbereitung.

## content

0. content
1. installation
2. usage
3. todo
4. license

## installation

1. clone this repo
2. install requirements
3. setup your config in config.py
4. run eisen.py to start a local server

### requirements

* python >3.6
* bottle.py

## usage

*todo write this*

### server setup

*todo write this*

## todo

* everything

## License

> ----------------------------------------------------------------------------
> "THE BEER-WARE LICENSE" (Revision 42):
> <feconi@posteo.de> wrote this thing. As long as you retain this notice you
> can do whatever you want with this stuff. If we meet some day, and you think
> this stuff is worth it, you can buy me a beer in return - Asterix
> ----------------------------------------------------------------------------

